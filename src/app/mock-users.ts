import { User } from './user';

export const USERS: User[] = [
  { gid: 100011, name: 'Dr Nice' },
  { gid: 100012, name: 'Narco' },
  { gid: 100013, name: 'Bombasto' },
  { gid: 100014, name: 'Celeritas' },
  { gid: 100015, name: 'Magneta' },
  { gid: 100016, name: 'RubberMan' },
  { gid: 100017, name: 'Dynama' },
  { gid: 100018, name: 'Dr IQ' },
  { gid: 100019, name: 'Magma' },
  { gid: 100020, name: 'Tornado' }
];