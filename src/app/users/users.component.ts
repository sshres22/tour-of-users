import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import {USERS} from '../mock-users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user: User = {
    gid: 100000,
    name: 'Windstorm'
  };
  constructor() { }

  ngOnInit() {
  }
  
  users = USERS;

  selectedUser: User;
  onSelect(user: User): void {
  this.selectedUser = user;
  }
}




